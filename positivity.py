import torch, decimal,random,time
import numpy as np
from transformers import BertTokenizer
from torchtext import data
from torchtext import datasets
from transformers import BertTokenizer, BertModel
import torch.optim as optim
import torch.nn as nn

SEED = 1234

tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')

len(tokenizer.vocab)

tokens = tokenizer.tokenize('Hello WORLD how ARE yoU?')

init_token = tokenizer.cls_token
eos_token = tokenizer.sep_token
pad_token = tokenizer.pad_token
unk_token = tokenizer.unk_token
init_token_idx = tokenizer.convert_tokens_to_ids(init_token)
eos_token_idx = tokenizer.convert_tokens_to_ids(eos_token)
pad_token_idx = tokenizer.convert_tokens_to_ids(pad_token)
unk_token_idx = tokenizer.convert_tokens_to_ids(unk_token)

max_input_length = tokenizer.max_model_input_sizes['bert-base-uncased']

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


bert = BertModel.from_pretrained('bert-base-uncased')
class BERTGRUSentiment(nn.Module):
    def __init__(self,
                 bert,
                 hidden_dim,
                 output_dim,
                 n_layers,
                 bidirectional,
                 dropout):
        
        super().__init__()
        
        self.bert = bert
        
        embedding_dim = bert.config.to_dict()['hidden_size']
        
        self.rnn = nn.GRU(embedding_dim,
                          hidden_dim,
                          num_layers = n_layers,
                          bidirectional = bidirectional,
                          batch_first = True,
                          dropout = 0 if n_layers < 2 else dropout)
        
        self.out = nn.Linear(hidden_dim * 2 if bidirectional else hidden_dim, output_dim)
        
        self.dropout = nn.Dropout(dropout)
        
    def forward(self, text):
        
        with torch.no_grad():
            embedded = self.bert(text)[0]
                
        _, hidden = self.rnn(embedded)
        
        if self.rnn.bidirectional:
            hidden = self.dropout(torch.cat((hidden[-2,:,:], hidden[-1,:,:]), dim = 1))
        else:
            hidden = self.dropout(hidden[-1,:,:])
                
        output = self.out(hidden)
        
        return output

HIDDEN_DIM = 256
OUTPUT_DIM = 1
N_LAYERS = 2
BIDIRECTIONAL = True
DROPOUT = 0.25

model = BERTGRUSentiment(bert,
                         HIDDEN_DIM,
                         OUTPUT_DIM,
                         N_LAYERS,
                         BIDIRECTIONAL,
                         DROPOUT)
def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)

#print(f'The model has {count_parameters(model):,} trainable parameters')

def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)

#print(f'The model has {count_parameters(model):,} trainable parameters')


model = model.to(device)

model.load_state_dict(torch.load('tut6-model.pt',map_location=torch.device('cpu')))

def positivetext(textin):
	positivetexts = ["xD", "lol", "lmao", "haha", "coolio", "Dope", "lit", "gucci",":)",":D"]
	for i in positivetexts:
		if i in textin:
			print("Quite positive!")
			return True
	return False

def predict_sentiment(model, tokenizer, sentence):
    model.eval()
    tokens = tokenizer.tokenize(sentence)
    tokens = tokens[:max_input_length-2]
    indexed = [init_token_idx] + tokenizer.convert_tokens_to_ids(tokens) + [eos_token_idx]
    tensor = torch.LongTensor(indexed).to(device)
    tensor = tensor.unsqueeze(0)
    prediction = torch.sigmoid(model(tensor))
    return prediction.item()

for i in range(999):
	userin = input("✍️  > ")
	if userin == "exit":
		exit(0)

	sentiment = decimal.Decimal((predict_sentiment(model, tokenizer, userin)))+decimal.Decimal("0.10")

	if positivetext(userin):
		sentiment += decimal.Decimal("0.45")

	if sentiment > decimal.Decimal("0.75"):
		feeling = "veryhappy"
		print("😀")

	elif sentiment > decimal.Decimal("0.50"):
		feeling = "happy"
		print("🙂")

	elif sentiment > decimal.Decimal("0.25"):
		feeling = "sad"
		print("😔")
	else:
		feeling = "angry"
		print("😠")

